# -*- coding: utf-8 -*-

"""
.. module:: cylegisapi.urls.urls
   :synopsis: 
.. moduleauthor:: Athanasios Rigas <admin@ithemis.gr>

"""

from django.conf.urls import url

from cylegisapi.views.apofasi import ApofasiView
from cylegisapi.views.apofasi_pdf import ApofasiPdfView
from cylegisapi.views.apofasi_docx import ApofasiDocxView
from cylegisapi.views.search import SearchView
from cylegisapi.views.recentapofaseis import RecentApofseisView

__author__ = 'Athanasios Rigas <admin@ithemis.gr>'

urlpatterns = [
    url(r'^apofaseis/(?P<model>[a-zA-Z_]+)/(?P<id>[0-9]+)/$',
        ApofasiView.as_view(),
        name='apofasi'),

    url(r'^apofaseis/(?P<model>[a-zA-Z_]+)/(?P<id>[0-9]+)/pdf/$',
        ApofasiPdfView.as_view(),
        name='apofasi_topdf'),

    url(r'^apofaseis/(?P<model>[a-zA-Z_]+)/(?P<id>[0-9]+)/docx/$',
        ApofasiDocxView.as_view(),
        name='apofasi_to_docx'),

    url(r'^search/$',
        SearchView.as_view(),
        name='search'),

    url(r'^recentapofaseis/$',
        RecentApofseisView.as_view(),
        name='recentapofaseis'),
]
