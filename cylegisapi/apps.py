# -*- coding: utf-8 -*-

"""
.. module:: cylegisapi.apps
   :synopsis:
.. moduleauthor:: Athanasios Rigas <admin@ithemis.gr>

"""

from django.apps import AppConfig

__author__ = 'Athanasios Rigas <admin@ithemis.gr>'


class CylegisConfig(AppConfig):
    """
    This helps you to customize the cylegisapi application
    name in the admin area.
    Don't forget to add the below in the cylegisapi.__init__.py:
    default_app_config = 'cylegisapi.apps.CylegisConfig'
    """

    name = 'cylegisapi'
    verbose_name = 'Cylegis Apofaseis'
