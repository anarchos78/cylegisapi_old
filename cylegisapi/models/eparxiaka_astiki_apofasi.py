# -*- coding: utf-8 -*-

"""
.. module:: cylegisapi.models.eparxiaka_astiki_apofasi
   :synopsis:
.. moduleauthor:: Athanasios Rigas <admin@ithemis.gr>

"""

from django.db import models

__author__ = 'Athanasios Rigas <admin@ithemis.gr>'


class EparxiakaAstikiApofasi(models.Model):
    """
    EparxiakaAstikiApofasi model
    """

    id = models.AutoField(primary_key=True,
                          blank=False,
                          null=False)
    apofasi_title = models.CharField(max_length=600,
                                     blank=False,
                                     null=False)
    case_num = models.CharField(max_length=50,
                                blank=False,
                                null=False)
    apofasi_year = models.IntegerField(blank=False,
                                       null=False)
    apofasi_month = models.IntegerField(blank=False,
                                        null=False)
    apofasi_body = models.TextField(blank=False,
                                    null=False)
    type = models.CharField(max_length=55,
                            blank=True,
                            null=True,
                            default='text')
    model = models.CharField(max_length=55,
                             blank=False,
                             null=False,
                             default='eparxiaka_astikes_apofaseis_dikastikes')
    url = models.CharField(max_length=255,
                           blank=True,
                           null=True)
    bin_con = models.BinaryField(blank=True,
                                 null=True)
    search_tag = models.CharField(max_length=55,
                                  blank=False,
                                  null=False,
                                  default='ΕΠΑΡΧΙΑΚΑ-ΑΣΤΙΚΕΣ')
    new_element = models.CharField(max_length=5,
                                   blank=False,
                                   null=False,
                                   default='false')
    last_modified = models.DateTimeField(blank=False,
                                         null=False,
                                         auto_now_add=True)
    is_english = models.CharField(max_length=10,
                                  blank=False,
                                  null=False,
                                  default='false')
    apofasi_day_inserted = models.IntegerField(blank=False,
                                               null=False)
    apofasi_month_inserted = models.IntegerField(blank=False,
                                                 null=False)

    @property
    def doc_id(self):
        """
        Returns a concatenated field useful for ExtJS
        """

        return "{}_{}".format(self.id, self.model)

    def __unicode__(self):
        """
        String representation of the model
        """

        return self.apofasi_title

    class Meta:
        app_label = 'cylegisapi'
        verbose_name_plural = 'Eparxiaka astikes apofaseis'
        db_table = 'eparxiaka_astikes_apofaseis_dikastikes'
        ordering = ['id']
