# -*- coding: utf-8 -*-

"""
.. module:: cylegisapi.models
   :synopsis: 
.. moduleauthor:: Athanasios Rigas <admin@ithemis.gr>

"""

from cylegisapi.models.add_apofasi import AddApofasi
from cylegisapi.models.eparxiaka_astiki_apofasi import EparxiakaAstikiApofasi
from cylegisapi.models.eparxiaka_enoikiaseon_apofasi import \
    EparxiakaEnoikiaseonApofasi
from cylegisapi.models.eparxiaka_ergatiko_apofasi import \
    EparxiakaErgatikoApofasi
from cylegisapi.models.eparxiaka_oikogeneiako_apofasi import \
    EparxiakaOikogeneiakoApofasi
from cylegisapi.models.eparxiaka_poiniki_apofasi import EparxiakaPoinikiApofasi
from cylegisapi.models.dioikitiki_apofasi import DioikitikiApofasi

__author__ = 'Athanasios Rigas <admin@ithemis.gr>'

__all__ = [
    'AddApofasi',
    'EparxiakaAstikiApofasi',
    'EparxiakaEnoikiaseonApofasi',
    'EparxiakaErgatikoApofasi',
    'EparxiakaOikogeneiakoApofasi',
    'EparxiakaPoinikiApofasi',
    'DioikitikiApofasi'
]
