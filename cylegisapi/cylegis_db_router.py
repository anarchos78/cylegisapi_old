# -*- coding: utf-8 -*-

"""
.. module:: cylegisapi.routers
   :synopsis: 
.. moduleauthor:: Athanasios Rigas <admin@ithemis.gr>

"""

__author__ = 'Athanasios Rigas <admin@ithemis.gr>'


class CylegisApiRouter():
    """
    A router to control all database operations on models in the
    cylegisapi application.
    """

    def db_for_read(self, model, **hints):
        """
        Attempts to read auth models go to cylegis database.
        """
        if model._meta.app_label == 'cylegisapi':
            return 'cylegis'
        return None

    def db_for_write(self, model, **hints):
        """
        Attempts to write auth models go to cylegis database.
        """
        if model._meta.app_label == 'cylegisapi':
            return 'cylegis'
        return None

    def allow_relation(self, obj1, obj2, **hints):
        """
        Allow relations if a model in the auth app is involved.
        """
        if obj1._meta.app_label == 'cylegisapi' or \
           obj2._meta.app_label == 'cylegisapi':
            return True
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        """
        Make sure the cylegisapi app only appears in the 'cylegis' database.
        """
        if app_label == 'cylegisapi':
            return db == 'cylegis'
        return None
