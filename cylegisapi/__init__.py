# -*- coding: utf-8 -*-

"""
.. module:: cylegisapi
   :synopsis: 
.. moduleauthor:: Athanasios Rigas <admin@ithemis.gr>

"""

__author__ = 'Athanasios Rigas <admin@ithemis.gr>'

default_app_config = 'cylegisapi.apps.CylegisConfig'
