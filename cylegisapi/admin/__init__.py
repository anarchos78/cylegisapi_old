# -*- coding: utf-8 -*-

"""
.. module:: cylegisapi.admin
   :synopsis: 
.. moduleauthor:: Athanasios Rigas <admin@ithemis.gr>

"""

from django.contrib import admin
from cylegisapi.admin.addapofasi_admin import AddApofasiAdmin
from cylegisapi.admin.eparxiaka_astiki_apofasi_admin import \
    EparxiakaAstikiApofasiAdmin
from cylegisapi.admin.eparxiaka_enoikiaseon_apofasi_admin import \
    EparxiakaEnoikiaseonApofasiAdmin
from cylegisapi.admin.eparxiaka_ergatiko_apofasi_admin import \
    EparxiakaErgatikoApofasiAdmin
from cylegisapi.admin.eparxiaka_oikogeneiako_apofasi_admin import \
    EparxiakaOikogeneiakoApofasiAdmin
from cylegisapi.admin.eparxiaka_poiniki_apofasi_admin import \
    EparxiakaPoinikiApofasiAdmin
from cylegisapi.admin.dioikitiki_apofasi_admin import DioikitikiApofasiAdmin

from cylegisapi.models import (
    AddApofasi,
    EparxiakaAstikiApofasi,
    EparxiakaEnoikiaseonApofasi,
    EparxiakaErgatikoApofasi,
    EparxiakaOikogeneiakoApofasi,
    EparxiakaPoinikiApofasi,
    DioikitikiApofasi
)

__author__ = 'Athanasios Rigas <admin@ithemis.gr>'


admin.site.register(AddApofasi, AddApofasiAdmin)
admin.site.register(EparxiakaAstikiApofasi, EparxiakaAstikiApofasiAdmin)
admin.site.register(EparxiakaEnoikiaseonApofasi,
                    EparxiakaEnoikiaseonApofasiAdmin)
admin.site.register(EparxiakaErgatikoApofasi, EparxiakaErgatikoApofasiAdmin)
admin.site.register(EparxiakaOikogeneiakoApofasi,
                    EparxiakaOikogeneiakoApofasiAdmin)
admin.site.register(EparxiakaPoinikiApofasi, EparxiakaPoinikiApofasiAdmin)
admin.site.register(DioikitikiApofasi, DioikitikiApofasiAdmin)
