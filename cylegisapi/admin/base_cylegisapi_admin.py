# -*- coding: utf-8 -*-

"""
.. module:: cylegis_user_item.admin.base_cylegis_user_item_admin
   :synopsis:
.. moduleauthor:: Athanasios Rigas <admin@ithemis.gr>

"""

from django.contrib import admin

__author__ = 'Athanasios Rigas <admin@ithemis.gr>'


class BaseCylegisUserItemAdmin(admin.ModelAdmin):
    """
    BaseCylegisUserItemAdmin: customized ModelAdmin from which all admins in
    cylegis_user_item app shall inherit
    """

    empty_value_display = '-empty-'

admin.site.site_header = 'CyLegis Administration'
admin.site.site_title = 'CyLegis Administration'
