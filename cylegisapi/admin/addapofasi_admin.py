# -*- coding: utf-8 -*-

"""
.. module:: cylegisapi.admin.addapofasi_admin.py
   :synopsis: 
.. moduleauthor:: Athanasios Rigas <admin@ithemis.gr>

"""

from cylegisapi.admin.base_cylegisapi_admin import BaseCylegisApiAdmin

__author__ = 'Athanasios Rigas <admin@ithemis.gr>'


class AddApofasiAdmin(BaseCylegisApiAdmin):
    """
    The model admin of AddApofasi model
    """

    search_fields = [
        'apofasi_title',
        'case_num',
        'apofasi_num',
        'apofasi_year',
        'apofasi_month',
        'apofasi_meros',
        'apofasi_body'
    ]

    list_display = [
        'id',
        'apofasi_title',
        'case_num',
        'apofasi_num',
        'apofasi_year',
        'apofasi_month',
        'apofasi_meros'
    ]

    list_filter = [
        'apofasi_year',
        'apofasi_month',
        'apofasi_meros',
        'apofasi_day_inserted',
        'apofasi_month_inserted'
    ]
