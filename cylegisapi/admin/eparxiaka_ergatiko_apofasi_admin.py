# -*- coding: utf-8 -*-

"""
.. module:: .eparxiaka_ergatiko_apofasi_admin
   :synopsis: 
.. moduleauthor:: Athanasios Rigas <admin@ithemis.gr>

"""

from cylegisapi.admin.base_cylegisapi_admin import BaseCylegisApiAdmin

__author__ = 'Athanasios Rigas <admin@ithemis.gr>'


class EparxiakaErgatikoApofasiAdmin(BaseCylegisApiAdmin):
    """
    The model admin of EparxiakaErgatikoApofasiAdmin model
    """

    search_fields = [
        'apofasi_title',
        'case_num',
        'apofasi_year',
        'apofasi_month',
        'apofasi_body'
    ]

    list_display = [
        'id',
        'apofasi_title',
        'case_num',
        'apofasi_year',
        'apofasi_month'
    ]

    list_filter = [
        'apofasi_year',
        'apofasi_month',
        'apofasi_day_inserted',
        'apofasi_month_inserted'
    ]
