# -*- coding: utf-8 -*-

"""
.. module:: cylegisapi.recentapofaseis
   :synopsis:
.. moduleauthor:: Athanasios Rigas <admin@ithemis.gr>

"""

from django.views.generic import View
from django.http import HttpResponse

from cylegisapi.models import (
    AddApofasi,
    EparxiakaAstikiApofasi,
    EparxiakaEnoikiaseonApofasi,
    EparxiakaErgatikoApofasi,
    EparxiakaOikogeneiakoApofasi,
    EparxiakaPoinikiApofasi,
    DioikitikiApofasi
)

import sys
import json
import datetime

from cylegisapi.utils import count_recentapofaseis

__author__ = 'Athanasios Rigas <admin@ithemis.gr>'


class RecentApofseisView(View):
    """
    RecentApofseisView class
    """

    def get(self, request, *args, **kwargs):
        """
        Returns resent apofaseis from the latest month
        """

        year = datetime.date.today().year
        month = datetime.date.today().month

        add_apofaseis = [{"doc_id": a.doc_id,
                          "ida": a.id,
                          "model": a.model,
                          "apofasi_year": a.apofasi_year,
                          "apofasi_month": a.apofasi_month,
                          "title": a.apofasi_title,
                          "case_num": a.case_num,
                          "apofasi_num": a.apofasi_num,
                          "apofasi_meros": a.apofasi_meros,
                          "type": a.type}
                         for a
                         in AddApofasi.
                         objects.filter(apofasi_year=year,
                                        apofasi_month_inserted=month).
                         order_by("apofasi_day_inserted")]

        eparxiaka_astikes_apofaseis = [{"doc_id": a.doc_id,
                                        "ida": a.id,
                                        "model": a.model,
                                        "apofasi_year": a.apofasi_year,
                                        "apofasi_month": a.apofasi_month,
                                        "title": a.apofasi_title,
                                        "case_num": a.case_num,
                                        "type": a.type}
                                       for a
                                       in EparxiakaAstikiApofasi.
                                       objects.filter(
                                       apofasi_year=year,
                                       apofasi_month_inserted=month
                                       ).order_by("apofasi_day_inserted")]

        eparxiaka_enoikiaseon_apofaseis = [{"doc_id": a.doc_id,
                                            "ida": a.id,
                                            "model": a.model,
                                            "apofasi_year": a.apofasi_year,
                                            "apofasi_month": a.apofasi_month,
                                            "title": a.apofasi_title,
                                            "case_num": a.case_num,
                                            "type": a.type}
                                           for a
                                           in EparxiakaEnoikiaseonApofasi.
                                           objects.filter(
                                           apofasi_year=year,
                                           apofasi_month_inserted=month
                                           ).order_by("apofasi_day_inserted")]

        eparxiaka_ergatiko_apofaseis = [{"doc_id": a.doc_id,
                                         "ida": a.id,
                                         "model": a.model,
                                         "apofasi_year": a.apofasi_year,
                                         "apofasi_month": a.apofasi_month,
                                         "title": a.apofasi_title,
                                         "case_num": a.case_num,
                                         "type": a.type}
                                        for a
                                        in EparxiakaErgatikoApofasi.
                                        objects.filter(
                                        apofasi_year=year,
                                        apofasi_month_inserted=month
                                        ).order_by("apofasi_day_inserted")]

        eparxiaka_oikogeneiako_apofaseis = [{"doc_id": a.doc_id,
                                             "ida": a.id,
                                             "model": a.model,
                                             "apofasi_year": a.apofasi_year,
                                             "apofasi_month": a.apofasi_month,
                                             "title": a.apofasi_title,
                                             "case_num": a.case_num,
                                             "type": a.type}
                                            for a
                                            in EparxiakaOikogeneiakoApofasi.
                                            objects.filter(
                                            apofasi_year=year,
                                            apofasi_month_inserted=month
                                            ).order_by("apofasi_day_inserted")]

        eparxiaka_poinikes_apofaseis = [{"doc_id": a.doc_id,
                                         "ida": a.id,
                                         "model": a.model,
                                         "apofasi_year": a.apofasi_year,
                                         "apofasi_month": a.apofasi_month,
                                         "title": a.apofasi_title,
                                         "case_num": a.case_num,
                                         "type": a.type}
                                        for a
                                        in EparxiakaPoinikiApofasi.
                                        objects.filter(
                                        apofasi_year=year,
                                        apofasi_month_inserted=month
                                        ).order_by("apofasi_day_inserted")]

        dioikitika_apofaseis_dikastikes = [{"doc_id": a.doc_id,
                                            "ida": a.id,
                                            "model": a.model,
                                            "apofasi_year": a.apofasi_year,
                                            "apofasi_month": a.apofasi_month,
                                            "title": a.apofasi_title,
                                            "case_num": a.case_num,
                                            "type": a.type}
                                           for a
                                           in DioikitikiApofasi.
                                           objects.filter(
                                           apofasi_year=year,
                                           apofasi_month_inserted=month
                                           ).order_by("apofasi_day_inserted")]
        recent_apofaseis = []

        recent_apofaseis.append({
            'dikastirio': 'aad_apofaseis_dikastikes',
            'apofaseis': add_apofaseis
        })

        recent_apofaseis.append({
            'dikastirio': 'eparxiaka_astikes_apofaseis_dikastikes',
            'apofaseis': eparxiaka_astikes_apofaseis
        })

        recent_apofaseis.append({
            'dikastirio': 'eparxiaka_enoikiaseon_apofaseis_dikastikes',
            'apofaseis': eparxiaka_enoikiaseon_apofaseis
        })

        recent_apofaseis.append({
            'dikastirio': 'eparxiaka_ergatiko_apofaseis_dikastikes',
            'apofaseis': eparxiaka_ergatiko_apofaseis
        })

        recent_apofaseis.append({
            'dikastirio': 'eparxiaka_oikogeneiako_apofaseis_dikastikes',
            'apofaseis': eparxiaka_oikogeneiako_apofaseis
        })

        recent_apofaseis.append({
            'dikastirio': 'eparxiaka_poinikes_apofaseis_dikastikes',
            'apofaseis': eparxiaka_poinikes_apofaseis
        })

        recent_apofaseis.append({
            'dikastirio': 'dioikitika_apofaseis_dikastikes',
            'apofaseis': dioikitika_apofaseis_dikastikes
        })

        if len(recent_apofaseis) == 0:
            resp = json.dumps({"data": 0,
                               "message": "Δεν έχουν εισαχθεί νέες αποφάσεις "
                                          "στην βάση δεδομένων ακόμα."
                               if sys.version_info[0] > 2
                               else u"Δεν έχουν εισαχθεί νέες αποφάσεις "
                                    u"στην βάση δεδομένων ακόμα.",
                               "dataset": count_recentapofaseis(recent_apofaseis),
                               "success": True},
                              ensure_ascii=False).encode('utf-8-sig')
        else:
            resp = json.dumps({"data": recent_apofaseis,
                               "message": "OK",
                               "dataset": count_recentapofaseis(recent_apofaseis),
                               "success": True},
                              ensure_ascii=False).encode('utf-8-sig')

        return HttpResponse(resp, content_type="application/json")
