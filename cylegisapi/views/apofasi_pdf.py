# -*- coding: utf-8 -*-

"""
.. module:: cylegisapi.views.apofasi_pdf
   :synopsis:
.. moduleauthor:: Athanasios Rigas <admin@ithemis.gr>

"""

from __future__ import unicode_literals

from django.views.generic import View
from django.http import HttpResponse
from django.http import StreamingHttpResponse

from cylegisapi.views.apofasi import ApofasiView
from cylegisapi.utils import DocumentPaths, replace_forbidden_chars, \
    create_pdf, define_print_suffix

from io import BytesIO
import codecs
import os
from unidecode import unidecode

__author__ = 'Athanasios Rigas <admin@ithemis.gr>'


class ApofasiPdfView(ApofasiView, View):
    """
    A view to return apofasi object as json or PDF
    """

    def render_to_response(self, context, **response_kwargs):
        """
        Returns the PDF
        """

        apofasi_obj = self.get_queryset()
        downloadcookie = self.request.GET.get('downloadcookie', None)

        if apofasi_obj:
            apofasi = apofasi_obj[0]

            apofasi_id = apofasi.get('id')
            apofasi_title = apofasi.get('apofasi_title')
            apofasi_model = apofasi.get('model')
            apofasi_body = apofasi.get('apofasi_body')
            apofasi_bin_con = apofasi.get('bin_con') or None

            # the below block is for pdf naming
            clean_apofasi_title = replace_forbidden_chars(apofasi_title)
            print_suffix = define_print_suffix(apofasi_model)

            file_title = '{}_{}_{}'.format(
                unidecode(clean_apofasi_title)[:13],
                unidecode(print_suffix),
                apofasi_id
            )

            if apofasi_bin_con:
                response = HttpResponse()
                response['Content-Description'] = 'File Transfer'
                response['Pragma'] = 'no-cache'
                response['Cache-Control'] = 'no-cache, no-store, ' \
                                            'must-revalidate'
                response['Content-Type'] = 'application/pdf'
                response['Content-Disposition'] = 'attachment; ' \
                                                  'filename="{}.pdf"'. \
                    format(file_title)
                response.set_cookie('downloadcookie', downloadcookie)

                buf = BytesIO(apofasi_bin_con)
                pdf = buf.getvalue()
                buf.close()

                response.write(pdf)

                return response
            else:
                templates_path = DocumentPaths().templates_path
                html_path = DocumentPaths().html_path
                pdf_path = DocumentPaths().pdf_path

                html_file_name = '{}.{}'.format(file_title, 'html')
                pdf_file_name = '{}.{}'.format(file_title, 'pdf')
                html_file_path = os.path.join(html_path, html_file_name)
                pdf_file_path = os.path.join(pdf_path, pdf_file_name)
                pdf_header_html = os.path.join(templates_path, 'header.html')
                pdf_footer_html = os.path.join(templates_path, 'footer.html')
                pdf_template = os.path.join(templates_path,
                                            'pdf_template.html')

                # Serve PDF from disc or else create it
                # Read read the article at:
                # voorloopnul.com/blog/serving-large-and-small-files-with-django
                # The solution below it's not optimize, instead use Apaches'
                # x-send-module to serve files
                if os.path.isfile(pdf_file_path):
                    response = StreamingHttpResponse(
                        (line for line in open(pdf_file_path, 'rb'))
                    )
                    response['Content-Description'] = 'File Transfer'
                    response['Pragma'] = 'no-cache'
                    response['Cache-Control'] = 'no-cache, no-store, ' \
                                                'must-revalidate'
                    response['Content-Type'] = 'application/pdf'
                    response['Content-Disposition'] = 'attachment; ' \
                                                      'filename={}'.\
                        format(pdf_file_name)
                    response['Content-Length'] = os.path.getsize(pdf_file_path)
                    response.set_cookie('downloadcookie', downloadcookie)

                    return response
                else:
                    apofasi_title = apofasi_title.\
                        replace('\n', '<br>').replace('&lt;br&gt;', '<br>')
                    apofasi_body = apofasi_body.\
                        replace('\n', '<br>').replace('&lt;br&gt;', '<br>')

                    with codecs.open(pdf_template, 'r', encoding='utf-8') \
                            as template:
                        data = template.read()

                    template = data.format(apofasi_title=apofasi_title,
                                           apofasi_body=apofasi_body)

                    if not os.path.isfile(html_file_path):
                        with codecs.open(html_file_path, 'w',
                                         encoding='utf-8') as fh:
                            fh.write(template)

                    create_pdf(html_file_path, pdf_file_path,
                               pdf_header_html, pdf_footer_html, None)

                    response = StreamingHttpResponse(
                        (line for line in open(pdf_file_path, 'rb'))
                    )
                    response['Content-Description'] = 'File Transfer'
                    response['Pragma'] = 'no-cache'
                    response['Cache-Control'] = 'no-cache, no-store, ' \
                                                'must-revalidate'
                    response['Content-Type'] = 'application/pdf'
                    response['Content-Disposition'] = 'attachment; ' \
                                                      'filename={}'.\
                        format(pdf_file_name)
                    response['Content-Length'] = os.path.getsize(pdf_file_path)
                    response.set_cookie('downloadcookie', downloadcookie)

                    return response
