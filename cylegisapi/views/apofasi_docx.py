# -*- coding: utf-8 -*-

"""
.. module:: cylegisapi.views.apofasi_docx
   :synopsis:
.. moduleauthor:: Athanasios Rigas <admin@ithemis.gr>

"""

from __future__ import unicode_literals

from django.views.generic import View
from django.http import HttpResponse
from django.http import StreamingHttpResponse

from cylegisapi.views.apofasi import ApofasiView
from cylegisapi.utils import DocumentPaths, replace_forbidden_chars, \
    valid_xml_char_ordinal, define_print_suffix

from io import BytesIO
import os
from unidecode import unidecode

from docx import Document

import re


__author__ = 'Athanasios Rigas <admin@ithemis.gr>'


class ApofasiDocxView(ApofasiView, View):
    """
    A view to return apofasi object as json or PDF
    """

    def render_to_response(self, context, **response_kwargs):
        """
        Returns the Docx
        """

        apofasi_obj = self.get_queryset()
        downloadcookie = self.request.GET.get('downloadcookie', None)

        if apofasi_obj:
            apofasi = apofasi_obj[0]

            apofasi_id = apofasi.get('id')
            apofasi_title = apofasi.get('apofasi_title')
            apofasi_model = apofasi.get('model')
            apofasi_body = apofasi.get('apofasi_body')
            apofasi_bin_con = apofasi.get('bin_con') or None

            # the below block is for pdf naming
            clean_apofasi_title = replace_forbidden_chars(apofasi_title)
            print_suffix = define_print_suffix(apofasi_model)

            file_title = '{}_{}_{}'.format(
                unidecode(clean_apofasi_title)[:13],
                unidecode(print_suffix),
                apofasi_id
            )

            if apofasi_bin_con:
                response = HttpResponse()
                response['Content-Description'] = 'File Transfer'
                response['Pragma'] = 'no-cache'
                response['Cache-Control'] = 'no-cache, no-store, ' \
                                            'must-revalidate'
                response['Content-Type'] = 'application/pdf'
                response['Content-Disposition'] = 'attachment; ' \
                                                  'filename="{}.pdf"'. \
                    format(file_title)
                response.set_cookie('downloadcookie', downloadcookie)

                buf = BytesIO(apofasi_bin_con)
                pdf = buf.getvalue()
                buf.close()

                response.write(pdf)
                return response
            else:
                templates_path = DocumentPaths().templates_path
                docx_path = DocumentPaths().docx_path
                docx_template = os.path.join(templates_path,
                                             'docx_template.docx')

                docx_file_name = '{}.{}'.format(file_title, 'docx')
                docx_file_path = os.path.join(docx_path, docx_file_name)

                if os.path.isfile(docx_file_path):
                    response = StreamingHttpResponse(
                        (line for line in open(docx_file_path, 'rb'))
                    )
                else:
                    apofasi_title = ''.join(c for c in apofasi_title
                                            if valid_xml_char_ordinal(c))
                    apofasi_body = ''.join(c for c in apofasi_body
                                           if valid_xml_char_ordinal(c))

                    apofasi_body = re.sub(
                        r'<(?:.|\n)*?>', '\n', apofasi_body, flags=re.MULTILINE
                    )

                    document = Document(docx_template)

                    p = document.add_paragraph()
                    p.add_run(apofasi_title).italic = True
                    p.add_run('\n\n')
                    p.add_run(apofasi_body).italic = True

                    document.save(docx_file_path)

                    response = StreamingHttpResponse(
                        (line for line in open(docx_file_path, 'rb'))
                    )

                response['Content-Description'] = 'File Transfer'
                response['Pragma'] = 'no-cache'
                response['Cache-Control'] = 'no-cache, no-store, ' \
                                            'must-revalidate'
                response['Content-Type'] = 'application/msword; ' \
                                           'charset=utf-8'
                response['Content-Disposition'] = 'attachment; ' \
                                                  'filename={}'.\
                    format(docx_file_name)
                response['Content-Length'] = os.path.\
                    getsize(docx_file_path)
                response.set_cookie('downloadcookie', downloadcookie)

                return response
