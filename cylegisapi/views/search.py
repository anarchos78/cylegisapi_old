# -*- coding: utf-8 -*-

"""
.. module:: cylegisapi.views.search
   :synopsis:
.. moduleauthor:: Athanasios Rigas <admin@ithemis.gr>

"""

from django.views.generic import View
from django.http import HttpResponse
from django.conf import settings

from cylegisapi.utils import extract_keywords

import json

from six import iteritems

try:
    import urllib.parse
    import urllib.request
except ImportError:
    import urllib

try:
    from urllib.parse import urlparse, urlencode
    from urllib.request import urlopen, Request
    from urllib.error import HTTPError
except ImportError:
    from urlparse import urlparse
    from urllib import urlencode
    from urllib2 import urlopen, Request, HTTPError

__author__ = 'Athanasios Rigas <admin@ithemis.gr>'


class SearchView(View):
    """
    SearchView class
    """

    def get(self, request, *args, **kwargs):
        """
        Handles the search request
        """

        q = request.GET.get('q', None)
        fq = request.GET.get('fq', None)

        if fq:
            query = {
                'q': q,
                'fq': fq
            }
        else:
            query = {
                'q': q
            }

        if hasattr(urllib, 'urlencode') and hasattr(urllib, 'urlopen'):
            values = {k: v.encode('utf-8') for k, v in iteritems(query)}
            data = urllib.urlencode(values)
            req = urllib.urlopen(settings.SOLR_URL_WITH_CREDENTIALS, data)
            solr_resp = req.read()
        else:
            values = {k: v for k, v in iteritems(query)}
            data = urllib.parse.urlencode(values).encode('ascii')

            password_mgr = urllib.request.HTTPPasswordMgrWithDefaultRealm()

            password_mgr.add_password(None,
                                      settings.SOLR_TOP_LEVEL_URL,
                                      settings.SOLR_USERNAME,
                                      settings.SOLR_PASSWORD)

            handler = urllib.request.HTTPBasicAuthHandler(password_mgr)
            opener = urllib.request.build_opener(handler)
            opener.open(settings.SOLR_TOP_LEVEL_URL)
            urllib.request.install_opener(opener)

            req = urllib.request.urlopen(
                settings.SOLR_URL_NO_CREDENTIALS, data)

            solr_resp = req.read().decode('utf-8')

        solr_resp_json = json.loads(solr_resp)

        docs = solr_resp_json['response']['docs']
        hl_docs_unordered = solr_resp_json['highlighting']

        # get the solr_id from docs in order to arrange "highlighted"
        # snippets in the right order based on solr_id presence order
        docs_solr_id = [doc.get('doc_id') for doc in docs]
        hl_docs_ordered = [hl_docs_unordered[doc_id]
                           for doc_id
                           in docs_solr_id]

        tmp_res = []
        for doc, hl_doc in zip(docs, hl_docs_ordered):
            if doc.get('content'):
                doc['content'] = hl_doc['content']
            else:
                doc['content'] = hl_doc['content'][-1] \
                    if len(hl_doc['content']) > 1 else hl_doc['content']

            if doc.get('title') and hl_doc.get('title'):
                doc['title'] = hl_doc['title']

            # Flat the entities
            if isinstance(doc['content'], list):
                doc['content'] = doc['content'][0]

            if isinstance(doc['title'], list):
                doc['title'] = doc['title'][0]

            if isinstance(doc['case_num'], list):
                doc['case_num'] = doc['case_num'][0]

            if 'apofasi_num' not in doc:
                doc['apofasi_num'] = ''
            else:
                if isinstance(doc['apofasi_num'], list):
                    doc['apofasi_num'] = doc['apofasi_num'][0]

            if not doc.get('kwlist'):
                if hasattr(urllib, 'urlencode') and hasattr(urllib, 'urlopen'):
                    doc['kwlist'] = extract_keywords(
                        str(hl_doc['content']),
                        'span', tag_class='nnee', ispython2=True)
                else:
                    doc['kwlist'] = extract_keywords(
                        str(hl_doc['content']),
                        'span', tag_class='nnee', ispython2=False)

            tmp_res.append(doc)

        resp = json.dumps(tmp_res, ensure_ascii=False).encode('utf-8-sig')

        return HttpResponse(resp, content_type="application/json")
