# -*- coding: utf-8 -*-

"""
.. module:: cylegisapi.views.apofasi
   :synopsis:
.. moduleauthor:: Athanasios Rigas <admin@ithemis.gr>

"""

from __future__ import unicode_literals

from django.views.generic.detail import BaseDetailView
from django.http import HttpResponse

import json
from io import BytesIO

from cylegisapi.utils import define_db_model, highlight_text, \
    replace_forbidden_chars, define_print_suffix

from unidecode import unidecode

__author__ = 'Athanasios Rigas <admin@ithemis.gr>'


class ApofasiView(BaseDetailView):
    """
    A view to return apofasi object as json or PDF
    """

    def get_object(self, queryset=None):
        """
        Get the object to work with
        """

        queryset = self.get_queryset()
        return queryset

    def get_queryset(self):
        """
        Get the queryset to return
        """

        apof_id = self.kwargs.get('id', None)
        apof_model = self.kwargs.get('model', None)

        if apof_model:
            self.model = define_db_model(apof_model)

        if apof_model == 'aad_apofaseis_dikastikes':
            apofasi = self.model.objects.filter(id=apof_id).values(
                'id',
                'apofasi_title',
                'case_num',
                'apofasi_num',
                'apofasi_year',
                'apofasi_month',
                'apofasi_meros',
                'apofasi_body',
                'type',
                'model',
                'url',
                'bin_con',
                'search_tag',
                'new_element',
                'is_english',
                'apofasi_day_inserted',
                'apofasi_month_inserted'
            )
        else:
            apofasi = self.model.objects.filter(id=apof_id).values(
                'id',
                'apofasi_title',
                'case_num',
                'apofasi_year',
                'apofasi_month',
                'apofasi_body',
                'type',
                'model',
                'url',
                'bin_con',
                'search_tag',
                'new_element',
                'is_english',
                'apofasi_day_inserted',
                'apofasi_month_inserted'
            )

        return apofasi

    def render_to_response(self, context, **response_kwargs):
        """
        Returns the data
        """

        apofasi_obj = self.get_queryset()

        if apofasi_obj:
            apofasi = apofasi_obj[0]
            apofasi_bin_con = apofasi.get('bin_con', None)

            if apofasi_bin_con:
                apofasi_id = apofasi.get('id')
                apofasi_title = apofasi.get('apofasi_title')
                apofasi_model = apofasi.get('model')

                # the below block is for pdf naming
                clean_apofasi_title = replace_forbidden_chars(apofasi_title)
                print_suffix = define_print_suffix(apofasi_model)

                file_title = '{}_{}_{}'.format(
                    unidecode(clean_apofasi_title)[:13],
                    unidecode(print_suffix),
                    apofasi_id
                )

                response = HttpResponse(content_type='application/pdf')
                response['Content-Disposition'] = 'inline; ' \
                                                  'filename="{}.pdf"'. \
                    format(file_title)

                buf = BytesIO(apofasi['bin_con'])
                pdf = buf.getvalue()
                buf.close()

                response.write(pdf)

                return response
            else:
                kwlist = self.request.GET.get('kwlist', None)

                if kwlist:
                    hl_terms = kwlist.split(',')

                    if 'apofasi_body' in apofasi.keys():
                        apofasi['apofasi_body'] = highlight_text(
                            apofasi.get('apofasi_body'), hl_terms
                        )

                    if 'apofasi_title' in apofasi.keys():
                        apofasi['apofasi_title'] = highlight_text(
                            apofasi.get('apofasi_title'), hl_terms
                        )

                # Change "id" to "ida"
                apofasi['ida'] = apofasi.pop('id')

                apofasi_json = json.dumps(apofasi, ensure_ascii=False). \
                    encode('utf-8-sig')

                return HttpResponse(
                    apofasi_json,
                    content_type='application/json'
                )
        else:
            error_json = {
                "error": "not found",
                "message": "invalid resource URI",
                "status": 404
            }

            return HttpResponse(json.dumps(error_json),
                                status=404,
                                content_type='application/json')
