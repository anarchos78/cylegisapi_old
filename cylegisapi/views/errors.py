# -*- coding: utf-8 -*-

"""
.. module:: cylegisapi.views.errors
   :synopsis: 
.. moduleauthor:: Athanasios Rigas <admin@ithemis.gr>

"""

from django.http import HttpResponse

import json

__author__ = 'Athanasios Rigas <admin@ithemis.gr>'


def bad_request(request):
    """
    400 error
    """

    response = HttpResponse()
    response.content_type = "application/json"
    resp = json.dumps({'status': 400,
                       'error': 'bad request'})
    response.status_code = 400
    response.content = resp

    return response


def permission_denied(request):
    """
    403 error
    """

    response = HttpResponse()
    response.content_type = "application/json"
    resp = json.dumps({'status': 403,
                       'error': 'bad request'})
    response.status_code = 403
    response.content = resp

    return response


def page_not_found(request):
    """
    404 error
    """

    response = HttpResponse()
    response.content_type = "application/json"
    resp = json.dumps({'status': 404,
                       'error': 'bad request'})
    response.status_code = 404
    response.content = resp

    return response


def server_error(request):
    """
    500 error
    """

    response = HttpResponse()
    response.content_type = "application/json"
    resp = json.dumps({'status': 500,
                       'error': 'bad request'})
    response.status_code = 500
    response.content = resp

    return response
