# -*- coding: utf-8 -*-

"""
.. module:: cylegisapi.utils
   :synopsis:
.. moduleauthor:: Athanasios Rigas <admin@ithemis.gr>

"""

from __future__ import unicode_literals

from cylegisapi.models import (
    AddApofasi,
    EparxiakaAstikiApofasi,
    EparxiakaEnoikiaseonApofasi,
    EparxiakaErgatikoApofasi,
    EparxiakaOikogeneiakoApofasi,
    EparxiakaPoinikiApofasi,
    DioikitikiApofasi
)

import sys
import re
import os
import platform
from bs4 import BeautifulSoup as Bs
import codecs
import zipfile
import shutil
from shutil import copy
try:
    from urllib.parse import unquote
except ImportError:
    from urllib import unquote


__author__ = 'Athanasios Rigas <admin@ithemis.gr>'


def define_db_model(model):
    """
    Returns the apofasi model string.
    :param model: a string
    :return: model
    """
    if model == 'aad_apofaseis_dikastikes':
        return AddApofasi
    elif model == 'eparxiaka_astikes_apofaseis_dikastikes':
        return EparxiakaAstikiApofasi
    elif model == 'eparxiaka_enoikiaseon_apofaseis_dikastikes':
        return EparxiakaEnoikiaseonApofasi
    elif model == 'eparxiaka_ergatiko_apofaseis_dikastikes':
        return EparxiakaErgatikoApofasi
    elif model == 'eparxiaka_oikogeneiako_apofaseis_dikastikes':
        return EparxiakaOikogeneiakoApofasi
    elif model == 'eparxiaka_poinikes_apofaseis_dikastikes':
        return EparxiakaPoinikiApofasi
    elif model == 'dioikitika_apofaseis_dikastikes':
        return DioikitikiApofasi
    else:
        return 'No db model found!!!'


def define_print_suffix(model):
    """
    Returns a suffix for printed apofasi.
    :param model: a string
    :return: model
    """
    if model == 'aad_apofaseis_dikastikes':
        return 'Α.Α.Δ'
    elif model == 'eparxiaka_astikes_apofaseis_dikastikes':
        return 'Αστικά'
    elif model == 'eparxiaka_enoikiaseon_apofaseis_dikastikes':
        return 'Ενοικιάσεων'
    elif model == 'eparxiaka_ergatiko_apofaseis_dikastikes':
        return 'Εργατικό'
    elif model == 'eparxiaka_oikogeneiako_apofaseis_dikastikes':
        return 'Οικογενειακό'
    elif model == 'eparxiaka_poinikes_apofaseis_dikastikes':
        return 'Ποινικό'
    elif model == 'dioikitika_apofaseis_dikastikes':
        return 'Διοικητικό'
    else:
        return 'No db model found!!!'


def highlight_text(st, kwlist, start_tag=None, end_tag=None):
    """
    Returns a string tagged with user specified tags
    :param st: the input string
    :param kwlist: the keyword list
    :param start_tag: the span start tag "<span class=\"nom\">"
    :param end_tag: the end span tag
    :return: string
    """
    if start_tag is None:
        start_tag = '<span class="nnee">'

    if end_tag is None:
        end_tag = '</span>'

    for kw in kwlist:
        if sys.version_info[0] > 2:
            st = re.sub(r'\b' + kw + r'\b',
                        '{}{}{}'.format(start_tag, kw, end_tag), st)
        else:
            kw = u'{}'.format(kw)
            st = re.sub(kw,
                        u'{}{}{}'.format(start_tag.decode('utf8'),
                                         kw,
                                         end_tag.decode('utf8')),
                        st,
                        count=0,
                        flags=re.U)
    return st


# def highlight_text(st, kwlist, start_tag=None, end_tag=None):
#     """
#     Returns a string tagged with user specified tags
#     :param st: the input string
#     :param kwlist: the keyword list
#     :param start_tag: the span start tag "<span class=\"nom\">"
#     :param end_tag: the end span tag
#     :return: string
#     """
#     if start_tag is None:
#         start_tag = '<span class="nom">'
#
#     if end_tag is None:
#         end_tag = '</span>'
#
#     for kw in kwlist:
#         if sys.version_info[0] > 2:
#             search = re.compile(r'\b({})\b'.format(kw), re.I)
#             st = search.sub('{}\\1{}'.format(start_tag, end_tag), st)
#         else:
#             kw = u'{}'.format(kw)
#             search = re.compile(r'\b({})\b'.format(kw), re.I)
#             st = search.sub('{}\\1{}'.format(start_tag, end_tag), st)
#     return st


def create_pdf(in_html_file=None, out_pdf_file=None,
               header_html=None, footer_html=None, quality=None):
    """
    Returns a PDF file
    :param in_html_file: the html file to be converted [full path]
    :param out_pdf_file: the PDF file to be generated [full path]
    :param footer_html: html template for PDF footer [full path]
    :param header_html: html template for PDF header [full path]
    :param quality: PDF quality
    :return: PDF file

    To install wkhtmltopdf on Ubuntu 14.04:
    sudo add-apt-repository ppa:ecometrica/servers
    sudo apt-get update
    sudo apt-get install wkhtmltopdf
    """

    # Check the running OS
    if 'linux' in (platform.system().lower(), platform.platform().lower()):
        wkhtmltopdf = 'wkhtmltopdf {0} {1} {2}'
    else:
        wkhtmltopdf = 'C:/wkhtmltopdf/bin/wkhtmltopdf.exe {0} {1} {2}'

    if quality == 1:
        # super quality no compression
        args_str = '--encoding utf-8 ' \
                   '--disable-smart-shrinking ' \
                   '--header-spacing 10 ' \
                   '--no-pdf-compression ' \
                   '--page-size A4 ' \
                   '--zoom 1 ' \
                   '-q -T 30.24mm -L 25.4mm -B 20.32mm -R 33.02mm ' \
                   '--header-html {} ' \
                   '--footer-html {}'.format(header_html, footer_html)
    elif quality == 2:
        # moderate quality some compression
        args_str = '--encoding utf-8 ' \
                   '--disable-smart-shrinking ' \
                   '--header-spacing 10 ' \
                   '--page-size A4 ' \
                   '--zoom 1 ' \
                   '-q -T 30.24mm -L 25.4mm -B 20.32mm -R 33.02mm ' \
                   '--header-html {} ' \
                   '--footer-html {}'.format(header_html, footer_html)
    else:
        # poor quality max compression
        args_str = '--encoding utf-8 ' \
                   '--page-size A4 ' \
                   '--zoom 1 ' \
                   '--header-spacing 10 ' \
                   '-q -T 30.24mm -L 25.4mm -B 20.32mm -R 33.02mm ' \
                   '--header-html {} ' \
                   '--footer-html {}'.format(header_html, footer_html)

    os.system(wkhtmltopdf.format(args_str, in_html_file, out_pdf_file))


def replace_forbidden_chars(name):
    """
    Replaces forbidden characters from string,
    to be on the safe side when creating filenames
    :param name: str
    :return: str
    """
    forbidden_chars = '*."/\[]:;|=, '
    for char in forbidden_chars:
        if char in name:
            name = name.replace(char, '_')

    if '__' in name:
        name = name.replace('__', '_')

    return name


class DocumentPaths():
    """
    A class holding application's full paths
    """
    @staticmethod
    def get_project_root_dir():
        scrip_normalized_path = os.path.normpath(os.path.realpath(__file__))
        os_seperator = os.sep
        split_path = scrip_normalized_path.split(os.sep)
        project_root = os_seperator.join(split_path[:-1])
        return project_root

    # root_path = run.app.root_path
    root_path = get_project_root_dir.__func__()
    documents_path = os.path.join(root_path, 'documents')
    templates_path = os.path.join(documents_path, 'templates')
    tmp_path = os.path.join(documents_path, 'tmp')
    html_path = os.path.join(documents_path, 'html')
    pdf_path = os.path.join(documents_path, 'pdf')
    docx_path = os.path.join(documents_path, 'docx')


class DocumentPathsAlter:
    """
    A class holding application's full paths
    """

    def __init__(self):
        """ Constructor """
        self.root_path = self.get_project_root_dir

    @property
    def get_project_root_dir(self):
        scrip_normalized_path = os.path.normpath(os.path.realpath(__file__))
        os_seperator = os.sep
        split_path = scrip_normalized_path.split(os.sep)
        project_root = os_seperator.join(split_path[:-1])
        return project_root

    @property
    def documents_path(self):
        """ Returns the documents_path path """
        return os.path.join(self.root_path, 'documents')

    @property
    def templates_path(self):
        """ Returns the templates path """
        return os.path.join(self.documents_path, 'templates')

    @property
    def tmp_path(self):
        """ Returns the templates path """
        return os.path.join(self.documents_path, 'tmp')

    @property
    def html_path(self):
        """ Returns the html path """
        return os.path.join(self.documents_path, 'html')

    @property
    def pdf_path(self):
        """ Returns the pdf path """
        return os.path.join(self.documents_path, 'pdf')

    @property
    def docx_path(self):
        """ Returns the docx path """
        return os.path.join(self.documents_path, 'docx')


def valid_xml_char_ordinal(c):
    """
    Replace non unicode characters
    See for explenation:
    stackoverflow.com/questions/2969044/python-string-escape-vs-unicode-escape
    :param c: string
    :return:
    """
    codepoint = ord(c)
    # conditions ordered by presumed frequency
    return (
        0x20 <= codepoint <= 0xD7FF or
        codepoint in (0x9, 0xA, 0xD) or
        0xE000 <= codepoint <= 0xFFFD or
        0x10000 <= codepoint <= 0x10FFFF
    )


def extract_keywords(text, tag, tag_class=None, ispython2=None):
    """
    Returns a list of keywords [uniquely occurred]
    :param text: the text
    :param tag: tag to match as string
    :param tag_class: the tag class as string
    :param ispython2: python version boolean flag
    :return: list
    """
    if tag_class is None:
        if ispython2 is True:
            return [k for k
                    in list(set([''.join(node.find_all(text=True)) for node
                                 in Bs(codecs.decode(text, 'unicode_escape'),
                                       'html.parser').find_all(tag)]))
                    if len(k) > 2]
        else:
            return [k for k
                    in list(set([''.join(node.find_all(text=True)) for node
                                 in Bs(text, 'html.parser').find_all(tag)]))
                    if len(k) > 2]
    else:
        if ispython2 is True:
            return [k for k
                    in list(set([''.join(node.find_all(text=True)) for node
                                 in Bs(codecs.decode(text, 'unicode_escape'),
                                       'html.parser').find_all(tag)]))
                    if len(k) > 2]
        else:
            return [k for k
                    in list(set([''.join(node.find_all(text=True)) for node
                                 in Bs(text, 'html.parser').find_all(tag)]))
                    if len(k) > 2]


def convert_greek_to_greeklish(datasource):
    poolGR = u"αβγδεζηθικλμνξοπρστυφχψω" \
             u"ΑΒΓΔΕΖΗΘΙΚΛΜΝΞΟΠΡΣΤΥΦΧΨΩςάέήίϊΐόύϋΰώΆΈΉΊΪΌΎΫΏ" \
             u"ἀἁἂἃἄἅἆἇἈἉἊἋἌἍἎἏἐἑἒἓἔἕἘἙἚἛἜἝἠἡἢἣἤἥἦἧἨἩἪἫἬἭἮἯ" \
             u"ἰἱἲἳἴἵἶἷἸἹἺἻἼἽἾἿὀὁὂὃὄὅὈὉὊὋὌὍὐὑὒὓὔὕὖὗὙὛὝὟὠὡὢὣὤὥὦὧ" \
             u"ὨὩὪὫὬὭὮὯὰάὲέὴήὶίὸόὺύὼώᾀᾁᾂᾃᾄᾅᾆᾇᾈᾉᾊᾋᾌᾍᾎᾏᾐᾑᾒᾓᾔᾕᾖᾗ" \
             u"ᾘᾙᾚᾛᾜᾝᾞᾟᾠᾡᾢᾣᾤᾥᾦᾧᾨᾩᾪᾫᾬᾭᾮᾯᾰᾱᾲᾳᾴᾶᾷᾸᾹᾺΆᾈἶ῁" \
             u"ῂῃῄῆῇῈΈῊΉῌ῍῎῏ῐῑῒΐῖῗῘῙῚΊ῝῞῟ῠῡῢΰῤῥῦῧῨῩῪΎῬ῭΅`ῲῳῴῶῷῸΌῺΏῼ"
    poolGL = "abgdezh8iklmn3oprstufx" \
             "4wABGDEZH8IKLMN3OPRSTYFX4WsaehiiiouuuwAEHIIOYYW" \
             "aaaaaaaaAAAAAAAAeeeeeeEEEEEEhhhhhhhhHHHHHHHH" \
             "iiiiiiiiIIIIIIIIooooooOOOOOOuuuuuuuuYYYYwwwwwwww" \
             "WWWWWWWWaaeehhiioouuwwaaaaaaaaAAAAAAAAhhhhhhhh" \
             "HHHHHHHHwwwwwwwwWWWWWWWWaaaaaaaAAAAA_____" \
             "hhhhhEEHHH___iiiiiiIIII___uuuurruuYYYYP___wwwwwOOWWW"

    pool = dict(zip(poolGR, poolGL))

    output_line = []
    for line in datasource:
        # try:
        #     line = line.encode("utf-8").decode()
        # except UnicodeDecodeError:
        #     line = line.encode("iso8859-7").decode()

        for character in line:
            if character in pool:
                output_line.append(pool[character])
            else:
                output_line.append(character)
    return "".join(output_line)


# def count_dict_items(d):
#     """
#     Count recent apofaseis dictionary items
#     :param d: dictionary
#     :return: int
#     """
#     count = 0
#     for dikastirio, apofaseis in d.items():
#         if isinstance(apofaseis, list):
#             count += len(apofaseis)
#     return count


def count_recentapofaseis(l):
    """
    Counts recentapofaseis
    :param l: list
    :return: int
    """

    count = 0
    for dikastirio in l:
        count += len(dikastirio.get('apofaseis'))
    return count


"""This is a sample function for zipping an entire directory into a zipfile"""

#This seems to work OK creating zip files on both windows and linux. The output
#files seem to extract properly on windows (built-in Compressed Folders feature,
#WinZip, and 7-Zip) and linux. However, empty directories in a zip file appear
#to be a thorny issue. The solution below seems to work but the output of
#"zipinfo" on linux is concerning. Also the directory permissions are not set
#correctly for empty directories in the zip archive. This appears to require
#some more in depth research.

#I got some info from:
#http://www.velocityreviews.com/forums/t318840-add-empty-directory-using-zipfile.html
#http://mail.python.org/pipermail/python-list/2006-January/535240.html

# Here's some samples of how you use this:
#
# zipdir("foo") #Just give it a dir and get a .zip file
# zipdir("foo", "foo2.zip") #Get a .zip file with a specific file name
# zipdir("foo", "foo3nodir.zip", False) #Omit the top level directory
# zipdir("../test1/foo", "foo4nopardirs.zip", False) #exclude some leading dirs
# zipdir("../test1/foo", "foo5pardir.zip") #Include some leading dirs


def zipdir(dirPath=None, zipFilePath=None, includeDirInZip=True):
    """Create a zip archive from a directory.

    Note that this function is designed to put files in the zip archive with
    either no parent directory or just one parent directory, so it will trim
    any leading directories in the filesystem paths and not include them inside
    the zip archive paths. This is generally the case when you want to just
    take a directory and make it into a zip file that can be extracted
    in different locations.

    Keyword arguments:

    dirPath -- string path to the directory to archive. This is the only
    required argument. It can be absolute or relative, but only one or zero
    leading directories will be included in the zip archive.

    zipFilePath -- string path to the output zip file. This can be an absolute
    or relative path. If the zip file already exists, it will be updated. If
    not, it will be created. If you want to replace it from scratch, delete it
    prior to calling this function. (default is computed as dirPath + ".zip")

    includeDirInZip -- boolean indicating whether the top level directory
    should be included in the archive or omitted. (default True)
    """
    if not zipFilePath:
        zipFilePath = dirPath + ".zip"
    if not os.path.isdir(dirPath):
        raise OSError("dirPath argument must point to a directory. "
            "'%s' does not." % dirPath)
    parentDir, dirToZip = os.path.split(dirPath)
    #Little nested function to prepare the proper archive path
    def trimPath(path):
        archivePath = path.replace(parentDir, "", 1)
        if parentDir:
            archivePath = archivePath.replace(os.path.sep, "", 1)
        if not includeDirInZip:
            archivePath = archivePath.replace(dirToZip + os.path.sep, "", 1)
        return os.path.normcase(archivePath)

    outFile = zipfile.ZipFile(zipFilePath, "w",
        compression=zipfile.ZIP_DEFLATED, allowZip64=True)
    for (archiveDirPath, dirNames, fileNames) in os.walk(dirPath):
        for fileName in fileNames:
            filePath = os.path.join(archiveDirPath, fileName)
            outFile.write(filePath, trimPath(filePath))
        #Make sure we get empty directories as well
        if not fileNames and not dirNames:
            zipInfo = zipfile.ZipInfo(trimPath(archiveDirPath) + "/")
            #some web sites suggest doing
            #zipInfo.external_attr = 16
            #or
            #zipInfo.external_attr = 48
            #Here to allow for inserting an empty directory.  Still TBD/TODO.
            outFile.writestr(zipInfo, "")
    outFile.close()


def create_pdf_group(user_id, collectiontitle, recordset):
    """
    :param user_id:
    :param collectiontitle:
    :param recordset:
    :return:
    """
    tmp_path = DocumentPaths.tmp_path
    user_dir = os.path.join(tmp_path, user_id)
    collection_dir = os.path.join(user_dir, collectiontitle)

    # Create the tmp directory if not exists
    if not os.path.exists(tmp_path):
        try:
            os.mkdir(tmp_path, mode=0o777)
        except:
            os.mkdir(tmp_path, 0o777)

    # The snippet below removes the contents of "user_dir"
    # so, if user tries to download the same collection after a change in it
    # the zip file will reflect the change
    for root, dirs, files in os.walk(user_dir):
        for f in files:
            os.unlink(os.path.join(root, f))
        for d in dirs:
            shutil.rmtree(os.path.join(root, d))

    # Create users' directory if not exists. It's not necessary to delete it
    if not os.path.exists(user_dir):
        try:
            os.mkdir(user_dir, mode=0o777)
        except:
            os.mkdir(user_dir, 0o777)

    # Create users' collection directory.
    # After successful collection download you should delete it
    if not os.path.exists(collection_dir):
        try:
            os.mkdir(collection_dir, mode=0o777)
        except:
            os.mkdir(collection_dir, 0o777)

    # Define the paths used by PDF creation process
    templates_path = DocumentPaths.templates_path
    html_path = DocumentPaths.html_path
    pdf_path = DocumentPaths.pdf_path

    # Templates paths
    pdf_header_html = os.path.join(templates_path, 'header.html')
    pdf_footer_html = os.path.join(templates_path, 'footer.html')
    pdf_template = os.path.join(templates_path, 'pdf_template.html')

    # {"apof_id": rec[1], "model": rec[2], "case_num": rec[4], "year": rec[5]}
    for entity in recordset:
        apof_id = entity['apof_id']
        model = define_db_model(entity['model'])
        apofasi_case_num = entity['case_num']
        apofasi_num = entity['apofasi_num']
        apofasi_year = entity['apofasi_year']

        apofasi = model.objects.get(id=apof_id)
        apofasi_title = apofasi.apofasi_title
        apofasi_body = apofasi.apofasi_body
        apofasi_bin_con = apofasi.bin_con

        html_file_name = '{}_{}.{}'.format(
            replace_forbidden_chars(apofasi_case_num), apofasi_year, 'html'
        )
        pdf_file_name = '{}_{}.{}'.format(
            replace_forbidden_chars(apofasi_case_num), apofasi_year, 'pdf'
        )
        html_file_path = os.path.join(html_path, html_file_name)
        pdf_file_path = os.path.join(pdf_path, pdf_file_name)

        # Copy PDF from disc or else create it
        if os.path.isfile(pdf_file_path):
            copy(os.path.join(pdf_file_path), collection_dir)
        else:
            if not apofasi_bin_con:
                apofasi_title = apofasi_title.\
                    replace('\n', '<br>').replace('&lt;br&gt;', '<br>')
                apofasi_body = apofasi_body.\
                    replace('\n', '<br>').replace('&lt;br&gt;', '<br>')

                with codecs.open(pdf_template, 'r', encoding='utf-8') \
                        as template:
                    data = template.read()

                template = data.format(apofasi_title=apofasi_title,
                                       apofasi_body=apofasi_body)

                if not os.path.isfile(html_file_path):
                    with codecs.open(html_file_path, 'w',
                                     encoding='utf-8') as fh:
                        fh.write(template)

                create_pdf(html_file_path, pdf_file_path,
                           pdf_header_html, pdf_footer_html, None)
                copy(os.path.join(pdf_file_path), collection_dir)
            else:
                # Serve the MySQL stored PDF in order to preserve format
                with open(pdf_file_path, 'wb') as fh:
                    fh.write(apofasi_bin_con)

                copy(os.path.join(pdf_file_path), collection_dir)

    # Compress collection directory
    zipdir(dirPath=collection_dir, includeDirInZip=True)

    zippath = os.path.join(user_dir, '{}.zip'.format(collectiontitle))

    return user_dir, zippath
