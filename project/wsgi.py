"""
WSGI config for cylegisapi project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.9/howto/deployment/wsgi/
"""

# Original configuartion - Start
import os

from django.core.wsgi import get_wsgi_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "project.settings")

application = get_wsgi_application()
# Original configuartion - End


# # =====================
# # wsgi.py file begin
#
# import os
# import sys
#
# from django.core.wsgi import get_wsgi_application
#
# # add the cylegisapi project path into the sys.path
# # sys.path.append('E:/Dev/Apache/htdocs/cylegisapi')
# sys.path.append('/home/cylegis.com/apps/cylegis_back/cylegisapi')
#
# # add the virtualenv site-packages path to the sys.path
# # sys.path.append('C:/Users/anarchos78/Envs/cylegisapi3/Lib/site-packages')
# sys.path.append('/Envs/cylegisapi/local/lib/python2.7/site-packages')
#
# # pointing to the project settings
# os.environ.setdefault("DJANGO_SETTINGS_MODULE", "project.settings")
#
# application = get_wsgi_application()
#
# # wsgi.py file end
# # ===================
