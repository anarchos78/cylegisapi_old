"""cylegisapi URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""

from django.conf.urls import include, url
from django.contrib import admin


handler400 = 'cylegisapi.views.errors.bad_request'
handler403 = 'cylegisapi.views.errors.permission_denied'
handler404 = 'cylegisapi.views.errors.page_not_found'
handler500 = 'cylegisapi.views.errors.server_error'

urlpatterns = [
    url(r'^api/v1/admin/', admin.site.urls)
]

urlpatterns += [
    url(r'^api/v1/', include('cylegisapi.urls.urls')),
    url(r'^api/v1/', include('cylegis_user_item.urls.urls')),
]

urlpatterns += (
    url(r'^admin/django-ses/', include('django_ses.urls')),
)
