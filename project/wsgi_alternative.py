"""
WSGI config for cylegisapi project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.9/howto/deployment/wsgi/
"""

# =====================
# wsgi.py file begin

import os
import sys
import site

# Add the site-packages of the chosen virtualenv to work with
site.addsitedir('/Envs/cylegisapi/local/lib/python2.7/site-packages')

# Add the app's directory to the PYTHONPATH
sys.path.append('/home/cylegis.com/apps/cylegis_back/cylegisapi')
sys.path.append('/home/cylegis.com/apps/cylegis_back/cylegisapi/project')

os.environ['DJANGO_SETTINGS_MODULE'] = 'project.settings'

# Activate your virtual env
activate_env = os.path.expanduser("/Envs/cylegisapi/bin/activate_this.py")
execfile(activate_env, dict(__file__=activate_env))

import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()

# wsgi.py file end
# ===================
