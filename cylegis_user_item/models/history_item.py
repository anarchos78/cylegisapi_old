# -*- coding: utf-8 -*-

"""
.. module:: cylegis_user_item.models.history_item
   :synopsis: 
.. moduleauthor:: Athanasios Rigas <admin@ithemis.gr>

"""

from django.db import models

from .user_session import UserSession

__author__ = 'Athanasios Rigas <admin@ithemis.gr>'


class HistoryItem(models.Model):
    """
    HistoryItem model
    """

    id = models.AutoField(primary_key=True,
                          blank=False,
                          null=False)
    user_id = models.IntegerField(blank=False,
                                  null=False)
    term = models.CharField(max_length=1500,
                            blank=False,
                            null=False)
    trimedterm = models.CharField(max_length=50,
                                  blank=False,
                                  null=False)
    searchfilter = models.CharField(max_length=512,
                                    blank=True,
                                    null=True)
    username = models.CharField(max_length=255,
                                blank=True,
                                null=True)
    name_f = models.CharField(max_length=255,
                              blank=True,
                              null=True)
    name_l = models.CharField(max_length=255,
                              blank=True,
                              null=True)
    dateInserted = models.DateTimeField(blank=False,
                                        null=False,
                                        auto_now_add=True)

    def save(self, *args, **kwargs):
        """
        Override model's save method
        """

        obj = UserSession.objects.get(user_id=self.user_id)

        self.username = obj.username
        self.name_f = obj.name_f
        self.name_l = obj.name_l

        super(HistoryItem, self).save(*args, **kwargs)

    def __unicode__(self):
        """
        String representation of the model
        """

        return self.trimedterm

    class Meta:
        app_label = 'cylegis_user_item'
        verbose_name_plural = 'History Items'
        db_table = 'history_item'
        ordering = ['id']
