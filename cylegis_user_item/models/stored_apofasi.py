# -*- coding: utf-8 -*-

"""
.. module:: cylegis_user_item.models.stored_apofasi
   :synopsis: 
.. moduleauthor:: Athanasios Rigas <admin@ithemis.gr>

"""

from django.db import models

__author__ = 'Athanasios Rigas <admin@ithemis.gr>'


class StoredApofasi(models.Model):
    """
    StoredApofasi model
    """

    id = models.AutoField(primary_key=True,
                          blank=False,
                          null=False)
    apofasiid = models.IntegerField(blank=False,
                                    null=False)
    user_id = models.IntegerField(blank=False,
                                  null=False)
    apofasi_title = models.CharField(max_length=600,
                                     blank=False,
                                     null=False)
    case_num = models.CharField(max_length=50,
                                blank=False,
                                null=False)
    apofasi_num = models.CharField(max_length=20,
                                   blank=False,
                                   null=False)
    apofasi_year = models.IntegerField(blank=False,
                                       null=False)
    apofasi_month = models.IntegerField(blank=False,
                                        null=False)
    apofasi_meros = models.IntegerField(blank=True,
                                        null=True)
    model = models.CharField(max_length=55,
                             blank=False,
                             null=False)
    search_tag = models.CharField(max_length=55,
                                  blank=False,
                                  null=False)
    collection = models.CharField(max_length=255,
                                  blank=False,
                                  null=False)
    dateInserted = models.DateTimeField(blank=False,
                                        null=False,
                                        auto_now_add=True)

    def __unicode__(self):
        """
        String representation of the model
        """

        return self.apofasi_title

    class Meta:
        app_label = 'cylegis_user_item'
        verbose_name_plural = 'Stored Apofaseis'
        db_table = 'stored_apofasi'
        ordering = ['id']

