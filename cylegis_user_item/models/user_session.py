# -*- coding: utf-8 -*-

"""
.. module:: cylegis_user_item.models.user_session
   :synopsis: 
.. moduleauthor:: Athanasios Rigas <admin@ithemis.gr>

"""

from django.db import models

__author__ = 'Athanasios Rigas <admin@ithemis.gr>'


class UserSession(models.Model):
    """
    UserSession model
    """

    # id = models.AutoField(primary_key=True,
    #                       blank=False,
    #                       null=False)
    user_id = models.AutoField(primary_key=True,
                               blank=False,
                               null=False)
    username = models.CharField(max_length=255,
                                blank=False,
                                null=False)
    name_f = models.CharField(max_length=255,
                              blank=False,
                              null=False)
    name_l = models.CharField(max_length=255,
                              blank=False,
                              null=False)
    phpsessid = models.CharField(max_length=255,
                                 blank=False,
                                 null=False)
    isloggedin = models.BooleanField(default=0)

    def __unicode__(self):
        """
        String representation of the model
        """

        return u"{} {}".format(self.name_l, self.name_f)

    class Meta:
        app_label = 'cylegis_user_item'
        verbose_name_plural = 'User Sessions'
        db_table = 'user_session'
        ordering = ['user_id']


