# -*- coding: utf-8 -*-

"""
.. module:: cylegis_user_item.admin
   :synopsis: 
.. moduleauthor:: Athanasios Rigas <admin@ithemis.gr>

"""

from django.contrib import admin
from cylegis_user_item.admin.history_admin import HistoryItemAdmin
from cylegis_user_item.admin.stored_apofasi_admin import StoredApofasiAdmin
from cylegis_user_item.admin.user_uession_admin import UserSessionAdmin

from cylegis_user_item.models import (
    HistoryItem,
    StoredApofasi,
    UserSession
)

__author__ = 'Athanasios Rigas <admin@ithemis.gr>'


admin.site.register(HistoryItem, HistoryItemAdmin)
admin.site.register(StoredApofasi, StoredApofasiAdmin)
admin.site.register(UserSession, UserSessionAdmin)
