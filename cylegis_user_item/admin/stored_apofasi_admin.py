# -*- coding: utf-8 -*-

"""
.. module:: cylegis_user_item.admin.stored_apofasi_admin
   :synopsis: 
.. moduleauthor:: Athanasios Rigas <admin@ithemis.gr>

"""

from cylegis_user_item.admin.base_cylegis_user_item_admin import \
    BaseCylegisUserItemAdmin

__author__ = 'Athanasios Rigas <admin@ithemis.gr>'


class StoredApofasiAdmin(BaseCylegisUserItemAdmin):
    """
    The model admin of StoredApofasi model
    """

    search_fields = [
        'user_id',
        'collection',
        'dateInserted'
    ]

    list_display = [
        'user_id',
        'apofasi_title',
        'collection',
        'dateInserted'
    ]

    list_filter = [
        'user_id'
    ]
