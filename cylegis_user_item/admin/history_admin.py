# -*- coding: utf-8 -*-

"""
.. module:: cylegis_user_item.admin.history_admin
   :synopsis: 
.. moduleauthor:: Athanasios Rigas <admin@ithemis.gr>

"""

from cylegis_user_item.admin.base_cylegis_user_item_admin import \
    BaseCylegisUserItemAdmin

__author__ = 'Athanasios Rigas <admin@ithemis.gr>'


class HistoryItemAdmin(BaseCylegisUserItemAdmin):
    """
    The model admin of HistoryItem model
    """

    search_fields = [
        'username'
    ]

    list_display = [
        'user_id',
        'username',
        'name_f',
        'name_l',
        'term',
        'searchfilter',
        'dateInserted'
    ]

    list_filter = [
        'username'
    ]
