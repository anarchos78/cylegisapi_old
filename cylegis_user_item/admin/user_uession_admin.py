# -*- coding: utf-8 -*-

"""
.. module:: cylegis_user_item.admin.user_uession_admin
   :synopsis: 
.. moduleauthor:: Athanasios Rigas <admin@ithemis.gr>

"""

from cylegis_user_item.admin.base_cylegis_user_item_admin import \
    BaseCylegisUserItemAdmin

__author__ = 'Athanasios Rigas <admin@ithemis.gr>'


class UserSessionAdmin(BaseCylegisUserItemAdmin):
    """
    The model admin of UserSession model
    """

    search_fields = [
        'user_id',
        'username',
        'name_f',
        'name_l',
        'phpsessid',
        'isloggedin'
    ]

    list_display = [
        'user_id',
        'username',
        'name_f',
        'name_l',
        'phpsessid',
        'isloggedin'
    ]

    list_filter = [
        'user_id',
        'isloggedin'
    ]
