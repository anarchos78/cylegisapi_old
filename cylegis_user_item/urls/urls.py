# -*- coding: utf-8 -*-

"""
.. module:: cylegis_user_item.urls.urls
   :synopsis: 
.. moduleauthor:: Athanasios Rigas <admin@ithemis.gr>

"""

from django.conf.urls import url

from cylegis_user_item.views.user_search_history import UserSearchHistory
from cylegis_user_item.views.user_stored_apofasi import UserStoredApofasi
from cylegis_user_item.views.store_to_collection import StoreToCollection
from cylegis_user_item.views.delete_from_collection import DeleteFromCollection
from cylegis_user_item.views.delete_collection import DeleteCollection
from cylegis_user_item.views.download_collection import DownloadCollection

__author__ = 'Athanasios Rigas <admin@ithemis.gr>'


urlpatterns = [
    url(r'^history/$',
        UserSearchHistory.as_view(),
        name='history'),

    url(r'^storedapofaseis/$',
        UserStoredApofasi.as_view(),
        name='storedapofaseis'),

    url(r'^collection/$',
        StoreToCollection.as_view(),
        name='collection-apofasi-store'),

    url(r'^collection/(?P<apofasi_id>\d+)/$',
        DeleteFromCollection.as_view(),
        name='collection-apofasi-delete'),

    url(r'^collection/group/(?P<collection>[\w-]+)/$',
        DeleteCollection.as_view(),
        name='collection-delete'),

    url(r'^collection/download/(?P<collection>.*)/$',
        DownloadCollection.as_view(),
        name='collection-download'),
]
