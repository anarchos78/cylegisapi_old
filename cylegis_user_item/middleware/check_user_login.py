# -*- coding: utf-8 -*-

"""
.. module:: cylegis_user_item.middleware.check_user_login
   :synopsis: 
.. moduleauthor:: Athanasios Rigas <admin@ithemis.gr>

"""

from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.conf import settings

from cylegis_user_item.models import UserSession

__author__ = 'Athanasios Rigas <admin@ithemis.gr>'


class CheckUserMiddleware(object):
    """
    Checks if user is logged in
    """

    def process_request(self, request):
        """
        Checks if user is logged in. If not, redirects to login page
        """

        if request.path.startswith(reverse('admin:index')):
            return None
        else:
            cookies = request.COOKIES

            username_from_cookies = cookies.get('user', None)
            phpsessid_from_cookies = cookies.get('PHPSESSID', None)
            logout_url = settings.AMEMBER_LOGOUT_URL
            # logout_url = 'http://127.0.0.1/amember/logout'

            if username_from_cookies:
                user_lookup = UserSession.objects.\
                    filter(phpsessid=phpsessid_from_cookies,
                           username=username_from_cookies).\
                    values('isloggedin', 'user_id')

                isloggedin = user_lookup[0].get('isloggedin')
                user_id = user_lookup[0].get('user_id')

                request.session['user_id'] = user_id

                if not isloggedin:
                    return HttpResponseRedirect(logout_url)
            else:
                request.session['user_id'] = None
                return HttpResponseRedirect(logout_url)
