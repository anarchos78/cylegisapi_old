# -*- coding: utf-8 -*-

"""
.. module:: cylegis_user_item.cylegis_user_item_db_router
   :synopsis: 
.. moduleauthor:: Athanasios Rigas <admin@ithemis.gr>

"""

__author__ = 'Athanasios Rigas <admin@ithemis.gr>'


class CylegisUserItemsRouter():
    """
    A router to control all database operations on models in the
    cylegis_users_items application.
    """

    def db_for_read(self, model, **hints):
        """
        Attempts to read auth models go to cylegis_users_items database.
        """
        if model._meta.app_label == 'cylegis_user_item':
            return 'cylegis_user_item'
        return None

    def db_for_write(self, model, **hints):
        """
        Attempts to write auth models go to cylegis_users_items database.
        """
        if model._meta.app_label == 'cylegis_user_item':
            return 'cylegis_user_item'
        return None

    def allow_relation(self, obj1, obj2, **hints):
        """
        Allow relations if a model in the auth app is involved.
        """
        if obj1._meta.app_label == 'cylegis_user_item' or \
           obj2._meta.app_label == 'cylegis_user_item':
            return True
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        """
        Make sure the cylegis_users_items app only appears in the
        'cylegis_users_items' database.
        """
        if app_label == 'cylegis_user_item':
            return db == 'cylegis_user_item'
        return None
