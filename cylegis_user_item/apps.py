# -*- coding: utf-8 -*-

"""
.. module:: cylegis_user_item.apps
   :synopsis:
.. moduleauthor:: Athanasios Rigas <admin@ithemis.gr>

"""

from django.apps import AppConfig

__author__ = 'Athanasios Rigas <admin@ithemis.gr>'


class CylegisUserItemsConfig(AppConfig):
    """
    This helps you to customize the cylegis_user_item application
    name in the admin area.
    Don't forget to add the below in the cylegis_user_item.__init__.py:
    default_app_config = 'cylegis_user_item.app.CylegisUserItemsConfig'
    """

    name = 'cylegis_user_item'
    verbose_name = 'Cylegis User Items'
