# -*- coding: utf-8 -*-

"""
.. module:: cylegis_user_item.views.download_collection
   :synopsis:
.. moduleauthor:: Athanasios Rigas <admin@ithemis.gr>

"""

from django.http import HttpResponse, StreamingHttpResponse
from django.views.generic import View

from cylegis_user_item.models import StoredApofasi

from cylegisapi.utils import create_pdf_group

import os
import sys
import json
from unidecode import unidecode
try:
    import urllib.parse
except ImportError:
    import urllib


__author__ = 'Athanasios Rigas <admin@ithemis.gr>'


class DownloadCollection(View):
    """
    DownloadCollection view
    """

    model = StoredApofasi

    def get(self, request, *args, **kwargs):
        """
        Returns a collection as a zip file
        """

        user_id = request.session.get('user_id')
        collection = self.kwargs.get('collection', None)
        # collection = u'{}'.format(self.kwargs.get('collection', None))

        count = StoredApofasi.objects.filter(user_id=user_id,
                                             collection=collection).count()

        if count:
            queryset = StoredApofasi.objects.\
                    filter(user_id=user_id,
                           collection=collection).\
                    values('apofasiid',
                           'model',
                           'case_num',
                           'apofasi_num',
                           'apofasi_year')

            # Check if is Python 3. If yes, use greeklish
            # In order to conduct the above check, we inspect if urllib
            # has 'parse' attribute
            # that's only available in Python 2
            if not hasattr(urllib, 'parse'):
                recordset = [{"apof_id": rec.get('apofasiid'),
                              "model": rec.get('model'),
                              "case_num": unidecode(rec.get('case_num')),
                              "apofasi_num": rec.get('apofasi_num'),
                              "apofasi_year": rec.get('apofasi_year')}
                             for rec
                             in queryset]
            else:
                recordset = [{"apof_id": rec.get('apofasiid'),
                              "model": rec.get('model'),
                              "case_num": rec.get('case_num'),
                              "apofasi_num": rec.get('apofasi_num'),
                              "apofasi_year": rec.get('apofasi_year')}
                             for rec
                             in queryset]

            # Check if is Python 3. If yes, use greeklish
            # In order to conduct the above check, we inspect
            # if urllib has 'parse' attribute
            # that's only available in Python 2
            # if not hasattr(urllib, 'parse'):
            #     collection = convert_greek_to_greeklish(collection)
            collection = unidecode(collection)

            ret = create_pdf_group(str(user_id), collection, recordset)

            response = StreamingHttpResponse(
                        (line for line in open(ret[1], 'rb'))
                    )
            response['Content-Description'] = 'File Transfer'
            response['Pragma'] = 'no-cache'
            response['Cache-Control'] = 'no-cache, no-store, ' \
                                        'must-revalidate'
            response['Content-Type'] = 'application/x-zip-compressed'
            if hasattr(urllib, 'parse'):
                response['Content-Disposition'] = \
                    "attachment; filename*=UTF-8''{}".format('{}.zip'.format(
                        urllib.parse.quote_plus(collection, encoding='utf-8')
                    )).replace('+', '_')
            else:
                response['Content-Disposition'] = \
                    "attachment; filename*=UTF-8''{}".format('{}.zip'.format(
                        urllib.quote_plus(collection))).replace('+', '_')
            response['Content-Length'] = os.path.getsize(ret[1])

            return response
        else:
            response = HttpResponse()
            response.content_type = "application/json"
            resp = json.dumps({"data": 0,
                               "message": "Δεν υπάρχει αυτή η ομάδα "
                                          "στην βάση δεδομένων."
                               if sys.version_info[0] > 2
                               else u"Δεν υπάρχει αυτή η ομάδα "
                                    u"στην βάση δεδομένων.",
                               "success": True},
                              ensure_ascii=False).encode('utf-8-sig')
            response.content = resp

            return response
