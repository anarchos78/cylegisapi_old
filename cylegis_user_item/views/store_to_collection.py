# -*- coding: utf-8 -*-

"""
.. module:: cylegis_user_item.views.store_to_collection
   :synopsis: 
.. moduleauthor:: Athanasios Rigas <admin@ithemis.gr>

"""

from django.http import HttpResponse
from django.views.generic import ListView, CreateView

from cylegis_user_item.models import StoredApofasi

from cylegisapi.utils import define_db_model

import sys
import json


__author__ = 'Athanasios Rigas <admin@ithemis.gr>'


class StoreToCollection(CreateView):
    """
    StoreToCollection view
    """

    model = StoredApofasi

    def post(self, request, *args, **kwargs):
        """
        Stores apofasi to user's collection if it's POST request
        """

        user_id = request.session.get('user_id')
        apof_id = self.request.POST.get('apof_id', None)
        apof_model = self.request.POST.get('model', None)
        collection = self.request.POST.get('collection', None)

        response = HttpResponse()
        response.content_type = "application/json"

        user_apofaseis_count = self.model.objects.filter(
            user_id=user_id
        ).count()

        if user_apofaseis_count > 119:
            resp = json.dumps({"data": 0,
                               "message": "Πρόκειται να ξεπεράσετε το"
                                          " επιτρεπόμενο όριο "
                                          "αποθηκευμένων αποφάσεων (120)."
                                          "<br>Για να μπορείτε να "
                                          "αποθηκεύσετε την απόφαση πρέπει"
                                          " να διαγράψετε <br>"
                                          "τουλάχιστον μια (1) απόφαση."
                               if sys.version_info[0] > 2
                               else u"Πρόκειται να ξεπεράσετε το "
                                    u"επιτρεπόμενο όριο αποθηκευμένων "
                                    u"αποφάσεων (120). <br>Για να μπορείτε"
                                    u" να αποθηκεύσετε την απόφαση πρέπει"
                                    u" να διαγράψετε <br>"
                                    u"τουλάχιστον μια (1) απόφαση.",
                               "success": True},
                              ensure_ascii=False).encode('utf-8-sig')
            response.content = resp
        else:
            # Check if an apofasi is going to be
            # stored twice in the same collection
            check = self.model.objects.filter(
                                              user_id=user_id,
                                              apofasiid=apof_id,
                                              model=apof_model,
                                              collection=collection
                                              ).count()

            if check:
                resp = json.dumps({"data": 0,
                                   "message": "Η απόφαση είναι ήδη "
                                              "αποθηκευμένη στην ομάδα "
                                              "<b>&quot;{}&quot;</b>."
                                              "<br>Μπορείτε να την "
                                              "αποθηκεύσετε μόνο σε άλλη"
                                              " ομάδα (υπόθεση).".
                                  format(collection)
                                   if sys.version_info[0] > 2
                                   else u"Η απόφαση είναι ήδη "
                                        u"αποθηκευμένη στην ομάδα "
                                        u"<b>&quot;{}&quot;</b>."
                                        u"<br>Μπορείτε να την "
                                        u"αποθηκεύσετε μόνο σε άλλη "
                                        u"ομάδα (υπόθεση).".
                                  format(collection),
                                   "success": True},
                                  ensure_ascii=False).encode('utf-8-sig')
                response.content = resp
            else:
                if apof_model:
                    db_model = define_db_model(apof_model)

                    if apof_model == 'aad_apofaseis_dikastikes':
                        apofasi = db_model.objects.filter(id=apof_id).values(
                            'apofasi_title',
                            'case_num',
                            'apofasi_num',
                            'apofasi_year',
                            'apofasi_month',
                            'apofasi_meros',
                            'search_tag'
                        )
                    else:
                        apofasi = db_model.objects.filter(id=apof_id).values(
                            'apofasi_title',
                            'case_num',
                            'apofasi_year',
                            'apofasi_month',
                            'search_tag'
                        )

                    apofasi = apofasi[0]
                    apofasi_title = apofasi.get('apofasi_title')
                    case_num = apofasi.get('case_num')
                    apofasi_num = apofasi.get('apofasi_num', None)
                    apofasi_meros = apofasi.get('apofasi_meros', None)
                    apofasi_year = apofasi.get('apofasi_year')
                    apofasi_month = apofasi.get('apofasi_month')
                    search_tag = apofasi.get('search_tag')

                    new_item = self.model(
                        user_id=user_id,
                        apofasiid=apof_id,
                        model=apof_model,
                        collection=collection,
                        apofasi_title=apofasi_title,
                        case_num=case_num,
                        apofasi_num=apofasi_num,
                        apofasi_year=apofasi_year,
                        apofasi_month=apofasi_month,
                        apofasi_meros=apofasi_meros,
                        search_tag=search_tag
                    )
                    new_item.save()

                    response = HttpResponse()
                    response.content_type = "application/json"

                    resp = json.dumps({"data": 0,
                                       "message": "Η απόφαση αποθηκεύτηκε "
                                                  "στην συλλογή σας."
                                       if sys.version_info[0] > 2
                                       else u"Η απόφαση αποθηκεύτηκε "
                                            u"στην συλλογή σας.",
                                       "success": True},
                                      ensure_ascii=False).encode('utf-8-sig')

                    response.content = resp

        return response
