# -*- coding: utf-8 -*-

"""
.. module:: cylegis_user_item.views.delete_from_collection
   :synopsis:
.. moduleauthor:: Athanasios Rigas <admin@ithemis.gr>

"""

from django.http import HttpResponse
from django.views.generic import DeleteView

from cylegis_user_item.models import StoredApofasi

import sys
import json


__author__ = 'Athanasios Rigas <admin@ithemis.gr>'


class DeleteCollection(DeleteView):
    """
    DeleteFromCollection view
    """

    model = StoredApofasi
    http_method_names = [u'delete']

    def delete(self, request, *args, **kwargs):
        """
        Deletes apofasi from user's collection
        """

        user_id = request.session.get('user_id')
        stored_apof_id = self.kwargs.get('apofasi_id', None)

        response = HttpResponse()
        response.content_type = "application/json"

        try:
            # self.model.objects.get(id=stored_apof_id).delete()
            obj = self.model.objects.filter(id=stored_apof_id,
                                            user_id=user_id)

            if obj:
                obj.delete()
                resp = json.dumps({"data": 0,
                                   "message": "Επιτυχής διαγραφή απόφασης."
                                   if sys.version_info[0] > 2
                                   else u"Επιτυχής διαγραφή απόφασης.",
                                   "success": True},
                                  ensure_ascii=False).encode('utf-8-sig')
            else:
                resp = json.dumps({"data": 0,
                                   "message": "Δεν υπάρχει αυτή η απόφαση."
                                   if sys.version_info[0] > 2
                                   else u"Δεν υπάρχει αυτή η απόφαση.",
                                   "success": True},
                                  ensure_ascii=False).encode('utf-8-sig')

            response.content = resp
        except self.model.DoesNotExist:
            resp = json.dumps({"data": 0,
                               "message": "Δεν υπάρχει αυτή η απόφαση."
                               if sys.version_info[0] > 2
                               else u"Δεν υπάρχει αυτή η απόφαση.",
                               "success": True},
                              ensure_ascii=False).encode('utf-8-sig')

            response.content = resp

        return response
