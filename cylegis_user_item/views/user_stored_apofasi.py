# -*- coding: utf-8 -*-

"""
.. module:: cylegis_user_item.views.user_stored_apofasi
   :synopsis: 
.. moduleauthor:: Athanasios Rigas <admin@ithemis.gr>

"""

from django.http import HttpResponse
from django.views.generic import View

from cylegis_user_item.models import StoredApofasi

import json

__author__ = 'Athanasios Rigas <admin@ithemis.gr>'


class UserStoredApofasi(View):
    """
    UserStoredApofasi view
    """

    def get(self, request, *args, **kwargs):
        """
        Returns user's stored apofaseis
        """

        user_id = request.session.get('user_id')

        stored_apofaseis = [{"id": i.id,
                             "apofasiid": i.apofasiid,
                             "user_id": i.user_id,
                             "apofasi_title": i.apofasi_title,
                             "case_num": i.case_num,
                             "apofasi_num": i.apofasi_num,
                             "apofasi_year": i.apofasi_year,
                             "apofasi_month": i.apofasi_month,
                             "apofasi_meros": i.apofasi_meros,
                             "model": i.model,
                             "search_tag": i.search_tag,
                             "collection": i.collection}
                            for i
                            in StoredApofasi.objects.filter(user_id=user_id).
                            order_by("id")]

        resp = json.dumps({"data": stored_apofaseis,
                           "message": "OK",
                           "dataset": len(stored_apofaseis),
                           "success": True},
                          ensure_ascii=False).encode('utf-8-sig')

        return HttpResponse(resp, content_type="application/json")
