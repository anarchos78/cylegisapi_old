# -*- coding: utf-8 -*-

"""
.. module:: cylegis_user_item.views.user_search_history
   :synopsis:
.. moduleauthor:: Athanasios Rigas <admin@ithemis.gr>

"""

from django.http import HttpResponse
from django.views.generic import View

from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt

from cylegis_user_item.models.history_item import HistoryItem

import json

__author__ = 'Athanasios Rigas <admin@ithemis.gr>'


@method_decorator(csrf_exempt, name='dispatch')
class UserSearchHistory(View):
    """
    UserSearchHistory view
    """

    def get(self, request, *args, **kwargs):
        """
        Returns user history items if it's GET request
        """

        user_id = request.session.get('user_id')

        history_items = [{"historyid": i.id,
                          "term": i.term,
                          "trimedterm": i.trimedterm}
                         for i
                         in HistoryItem.objects.filter(user_id=user_id).
                         order_by("-id")]

        resp = json.dumps({"data": history_items,
                           "message": "OK",
                           "dataset": len(history_items),
                           "success": True},
                          ensure_ascii=False).encode('utf-8-sig')

        return HttpResponse(resp, content_type="application/json")

    def post(self, request, *args, **kwargs):
        """
        Stores user search items if it's POST request
        """

        user_id = request.session.get('user_id')

        request_body = self.request.body
        obj = json.loads(request_body.decode('utf-8'))
        data = obj.get('data')

        if isinstance(data, dict):
            term = data.get('term', None)
        else:
            term = data[-1].get('term', None)

        if isinstance(data, dict):
            searchfilter = data.get('searchfilter', None)
        else:
            searchfilter = data[-1].get('searchfilter', None)

        response = HttpResponse()
        response.content_type = "application/json"

        if term:
            encoded_term = u'{}'.format(term)
            items_count = HistoryItem.objects.count()

            if items_count > 49:
                items = HistoryItem.objects.filter(user_id=user_id)[:1]

                for item in items:
                    item.delete()

            if len(encoded_term) > 47:
                trimedterm = u'{}...'.format(encoded_term[:47])
            else:
                trimedterm = encoded_term

            new_item = HistoryItem(user_id=user_id,
                                   term=term,
                                   trimedterm=trimedterm,
                                   searchfilter=searchfilter)
            new_item.save()

            history_items = [{"historyid": i.id,
                              "term": i.term,
                              "trimedterm": i.trimedterm}
                             for i
                             in HistoryItem.objects.filter(user_id=user_id).
                                 order_by("-id")]

            resp = json.dumps({"data": history_items,
                               "message": "OK",
                               "dataset": len(history_items),
                               "success": True},
                              ensure_ascii=False).encode('utf-8-sig')

            response.content = resp
        else:
            response.content = json.dumps({"data": 0,
                                           "message": "redirect",
                                           "success": True})

        return response
