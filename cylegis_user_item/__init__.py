# -*- coding: utf-8 -*-

"""
.. module:: cylegis_user_item
   :synopsis: 
.. moduleauthor:: Athanasios Rigas <admin@ithemis.gr>

"""

__author__ = 'Athanasios Rigas <admin@ithemis.gr>'

default_app_config = 'cylegis_user_item.apps.CylegisUserItemsConfig'
